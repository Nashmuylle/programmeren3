﻿using System;

namespace les3_opdracht_werken_met_gegevens
{
    class Program
    {
        static void Main(string[] args)
        {
            StructIsByValue structIsByValue;
            structIsByValue.getalVanStruct = 0;
            StructVersusClass structVersusClass = new StructVersusClass();
            structVersusClass.classGetal = 0;
            Console.WriteLine("Een Struct aan het werk");
            for (int i = 0; i < 100; i++)
            {
                structVersusClass.TabelMetGetallenStruct(structIsByValue); //is telkens maar één omdat het steeds op een nieuw stuk "papier" is
            }
            Console.WriteLine("\nEen Klasse aan het werk");
            for(int i = 0; i < 100; i++)
            {
                structVersusClass.TabelMetGetallenClass(structVersusClass); //Vermeerdert omdat het steeds hetzelfde stuk "papier" is
            }

            
        }
    }
}
