﻿using System;
using System.Collections.Generic;
using System.Text;

namespace les3_opdracht_werken_met_gegevens
{
    struct StructIsByValue
    {
        internal int getalVanStruct;
    }
    class StructVersusClass
    {

        public int classGetal = 1;
        public StructIsByValue TabelMetGetallenStruct(StructIsByValue testGetal)
        {
                testGetal.getalVanStruct++;
                if(testGetal.ToString().Contains("0")) //Dan moeten we de nieuwe lijn toevoegen
                {
                    Console.Write($"{testGetal.getalVanStruct.ToString(),10} \n");
                }
                else
                {
                    Console.Write($"{testGetal.getalVanStruct.ToString(),10} |");
                }
            return testGetal;
        }
        public StructVersusClass TabelMetGetallenClass(StructVersusClass classGetal)
        {
            classGetal.classGetal++;
            if (classGetal.classGetal.ToString().Contains("0")) //Dan moeten we de nieuwe lijn toevoegen
            {
                Console.Write($"{classGetal.classGetal.ToString(),10} \n");
            }
            else
            {
                Console.Write($"{classGetal.classGetal.ToString(),10} |");
            }
            return classGetal;
        }
    }
}

