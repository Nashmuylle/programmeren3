﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL;
using System.IO;
using System.Xml.Serialization;
using Helpers;

namespace DAL //De 2de laag in het "framework?" 
{
    class TryOut
    {
        public TryOut()
        {

        }
        public static string ReadPostcodesFromCSVFile()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = @"Data/Postcodes.csv";
            bestand.Lees();
            return bestand.Text;
        }
        public static List<Postcode> GetPostcodeList() //Gaat een lijst van postcodes geven
        {

            string[] postcodes = ReadPostcodesFromCSVFile().Split('\n');
            List<Postcode> list = new List<Postcode>(); //Er worst eerst een nieuwe lijst aangemaakt
            foreach (string s in postcodes) //Voor elke regel tekst in de array
            {
                if (s.Length > 0)
                {
                    list.Add(PostcodeCsvToObject(s)); //Gaan we een object aan de list toevoegen 
                }
            }
            return list;
        }
        public static Postcode PostcodeCsvToObject(string line) //Het parameter komt mee uit de string van het csv bestand. Zie hierboven de foreach
        {
            Postcode postcode = new Postcode();
            string[] values = line.Split('|'); //Overal waar dit voorkomt gaan we het opsplitsen.  Zo kunnen we vervolgens apart toevoegen, zie hieronder.
            postcode.Code = values[0]; //Eerste ding dat is opgesplits. Vandaar 0 --> het eerste ding van de array
            postcode.Plaats = values[1];
            postcode.Provincie = values[2];
            postcode.Localite = values[3];
            postcode.Province = values[4];
            return postcode;
        }
        public static BLL.Postcode[] GetPostcodeArray()
        {
            string[] lines = ReadPostcodesFromCSVFile().Split('\n');
            BLL.Postcode[] postcodes = new BLL.Postcode[lines.Length];
            int i = 0;
            foreach (string s in lines)
            {
                if (s.Length > 0)
                {
                    postcodes[i++] = PostcodeCsvToObject(s);
                }
            }
            return postcodes;
        }
        public static void SerializeCsvToXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BLL.Postcode[]));
            BLL.Postcode postcode = new BLL.Postcode();
            TextWriter writer = new StreamWriter(@"Data/Postcode.xml");
            //De serializer werkt niet voor een generieke lijst en ook niet voor ArrayList
            BLL.Postcode[] postcodes = GetPostcodeArray();
            serializer.Serialize(writer, postcodes);
            writer.Close();
        }
    }
}
