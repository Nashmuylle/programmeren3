﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL; //We gebruiken de juiste laag
using DAL;
namespace PL
{
    class Presentation
    {
        public Presentation()
        {

        }
        public static void ListPostcodes(List<Postcode> list)
        {
            foreach (Postcode postcode in list)
            {
                // One of the most versatile and useful additions to the C# language in version 6
                // is the null conditional operator ?.Post           
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}",
                    postcode?.Code, //Vraagteken punt is "als het geen nul is dan mag je het gebruiken". Anders geeft het gewoon , dus lege strings
                    postcode?.Plaats,
                    postcode?.Provincie,
                    postcode?.Localite,
                    postcode?.Province);
            }
        }
    }
}
