﻿using System;
using PL;
using BLL;
using DAL;

namespace Serialisatie
{
    class Program
    {
        static void Main(string[] args)
        {
            // Lees het csv bestand in en toon ingelezen string
            Console.WriteLine(TryOut.ReadPostcodesFromCSVFile());
            // zet stringvoorstelling postcodes om in een lijst
            // van postcode-objecten en toon de lijst in de console
            Presentation.ListPostcodes(TryOut.GetPostcodeList());
            Console.ReadKey();
        }
    }
}
