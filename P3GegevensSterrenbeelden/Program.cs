﻿using System;

namespace P3GegevensSterrenbeelden
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("In what month(number) are you born?");
            int month = int.Parse(Console.ReadLine());


            
            switch (month-1) //-1 omdat het tellen van een lijst vanuit 0 begint
            {
                case (int)AstrologicalSign.Capricorn:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Capricorn}");
                    break;
                case (int)AstrologicalSign.Aquarius:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Aquarius}");
                    break;
                case (int)AstrologicalSign.Pisces:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Pisces}");
                    break;
                case (int)AstrologicalSign.Taurus:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Taurus}");
                    break;
                case (int)AstrologicalSign.Gemini:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Gemini}");
                    break;
                case (int)AstrologicalSign.Cancer:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Cancer}");
                    break;
                case (int)AstrologicalSign.Leo:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Leo}");
                    break;
                case (int)AstrologicalSign.Virgo:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Virgo}");
                    break;
                case (int)AstrologicalSign.Libra:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Libra}");
                    break;
                case (int)AstrologicalSign.Scorpio:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Scorpio}");
                    break;
                case (int)AstrologicalSign.Sagittarius:
                    Console.WriteLine($"Je bent waarsschijnlijk een {AstrologicalSign.Sagittarius}");
                    break;

            }
        }
    }
}
