﻿using System;
using MySql.Data.MySqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Collections.Generic;

namespace Adodotnet
{
    class Learning
    {
        public static void TestMySqlConnector()
        {
            MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection();

            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            IConfiguration configuration = builder.Build();
            string connectionString = string.Format("server={0};user id={1};password={2};port={3};database={4};SslMode={5};",
                configuration["connection:server"],
                configuration["connection:userid"],
                configuration["connection:password"],
                configuration["connection:port"],
                configuration["connection:database"],
                configuration["connection:SslMode"]);
            Console.WriteLine(connectionString);
            connection.ConnectionString = connectionString;
            //

            using (connection)
            {
                MySqlCommand command = new MySqlCommand();
                string sqlStatement = "select Name, Id from EventCategory;";
                command.Connection = connection;
                command.CommandText = sqlStatement;
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Console.WriteLine("{0}\t{1}", reader["Name"],
                            reader["Id"]);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();
            }
            Console.ReadKey();
        }
        public static void FricFracDalTest()
        {
            Console.WriteLine("Fric-frac DAL test");
            Adodotnet.Dal.EventCategory dal = new Adodotnet.Dal.EventCategory();
            List<Adodotnet.Bll.EventCategory> list = dal.ReadAll();
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            foreach (Adodotnet.Bll.EventCategory item in list)
                Console.WriteLine("{0} {1}", item.Id, item.Name);
            dal.ReadOne(4);
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            dal.ReadOne(200);
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
        }
    }
}