﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using FricFracMyWay.Models;
namespace FricFracMyWay.DAL
{
    public class PostcodeXml : IPostcode
    {
        public PostcodeXml()
        {
        }
        public Models.Postcode Postcode { get; set; }
        public string Message { get; set; }
        private string connectionString = @"Data/Postcode";
        public string ConnectionString
        {
            get
            {
                return connectionString + ".xml";
            }
            set
            {
                connectionString = value;
            }
        }

        public PostcodeXml(Models.Postcode postcode)
        {
            Postcode = postcode;
        }

        public PostcodeXml(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public bool Create()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Models.Postcode[]));
                TextWriter writer = new StreamWriter(ConnectionString);
                Models.Postcode[] postcodes = Postcode.List.ToArray();
                serializer.Serialize(writer, postcodes);
                writer.Close();
                Message = $"Bestand {ConnectionString} is met succes geserialiseerd.";
                return true;
            }
            catch (Exception e)
            {
                Message = $"Het bestand {ConnectionString} s niet gedeserialiseerd.\nFoutmelding {e.Message}.";
                return false;
            }
        }

        public bool ReadAll()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Models.Postcode[]));
                StreamReader file = new System.IO.StreamReader(ConnectionString);
                Models.Postcode[] postcodes = (Models.Postcode[])serializer.Deserialize(file);
                file.Close();
                // array converteren naar List
                Postcode.List = new List<Models.Postcode>(postcodes);
                Message = $"Bestand {ConnectionString} is met succes gedeserialiseerd.";
                return true;
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                Message = $"Het bestand {ConnectionString} s niet gedeserialiseerd.\nFoutmelding {e.Message}.";
                return false;
            }
        }
    }
}
