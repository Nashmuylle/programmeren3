﻿using System;
namespace FricFracMyWay.DAL
{
    public interface IPostcode
    {
        Models.Postcode Postcode { get; set; }
        string Message { get; set; }
        string ConnectionString { get; set; }
        bool Create();
        bool ReadAll();
    }
}
