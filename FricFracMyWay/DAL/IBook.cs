﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FricFracMyWay.DAL
{
    public interface IBook
    {
        Models.Book Book { get; set; }
        string Message { get; set; }
        string ConnectionString { get; set; }
        bool Create();
        bool ReadAll();
    }
}
