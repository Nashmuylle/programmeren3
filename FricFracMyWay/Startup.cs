using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using FricFracMyWay.Models;

namespace FricFracMyWay
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            //De injectie van de postcodes
            services.AddSingleton<FricFracMyWay.DAL.IPostcode>
                (p => new FricFracMyWay.DAL.PostcodeXml(new FricFracMyWay.Models.Postcode()));
            services.AddTransient(p => new Controllers.PostcodeController(new FricFracMyWay.DAL.PostcodeXml()));
            //Einde van de injectie van de postcodesXML
            //De injectie van de bookXML
            /*
            services.AddSingleton<FricFracMyWay.DAL.IBook>
                (p => new FricFracMyWay.DAL.BookXml(new FricFracMyWay.Models.Book()));
            services.AddTransient(p => new Controllers.BookController(new FricFracMyWay.DAL.BookXml()));
            */
            services.AddDbContext<MyEntities>(o => o.UseMySql(Configuration.GetConnectionString("MyEntities"))); //Deze service hebben we zelf toegevoegd. Dit zal zeggen dat we de service Dbcontext gaan gebruiken met myEntities.

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}