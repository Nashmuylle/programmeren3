using System.Runtime;
using Microsoft.EntityFrameworkCore;

namespace FricFracMyWay.Models
{
    public partial class MyEntities : DbContext //Hier gaan we de entititeiten van de databank getten en setten.
    {
        //Zie fric-frac model refactoring hoe dit juist moet zijn, momenteel nog op eigen manier
        public MyEntities(DbContextOptions<MyEntities> options)
            : base(options)
        {
        } //Dit geeft de configuratie file mee

        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Events> Events { get; set; }
        public virtual DbSet<EventCategory> EventCategory { get; set; }
        public virtual DbSet<EventTopic> EventTopic { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }
    }
}