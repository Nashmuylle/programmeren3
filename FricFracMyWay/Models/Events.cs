using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace FricFracMyWay.Models
{
    public partial class Events
    {
        public Events()
        {
            
        }
        [Required]
        [Column(TypeName = "varchar(1024)")]
        [FromForm(Name = "EventsDescription")]
        public string Description { get; set; }
        [Column(TypeName = "datetime")]
        [FromForm(Name = "EventsEnd")]
        public DateTime? Ends { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Events-EventCategory")]
        public int? EventCategoryId { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Events-EventTopicId")]
        public int? EventTopicId { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "EventsId")]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(255)")]
        [FromForm(Name = "EventsImage")]
        public string Image { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        [FromForm(Name = "EventsLocation")]
        public string Location { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        [FromForm(Name = "Events-Name")]
        public string Name { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        [FromForm(Name = "EventsOrganiserDescription")]
        public string OrganiserDescription { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        [FromForm(Name = "EventsOrganiserName")]
        public string OrganiserName { get; set; }
        [Column(TypeName = "datetime")]
        [FromForm(Name = "EventsStarts")]
        public DateTime? Starts { get; set; }

        [ForeignKey(nameof(EventCategoryId))]
        [InverseProperty("Events")]
        public virtual EventCategory EventCategory { get; set; }
        [ForeignKey(nameof(EventTopicId))]
        [InverseProperty("Events")]
        public virtual EventTopic EventTopic { get; set; }
    }
}