using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace FricFracMyWay.Models
{
    public partial class EventCategory
    {
        public EventCategory()
        {
        }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        [FromForm(Name = "EventCategory-Name")]
        public string Name { get; set; }

        [InverseProperty("EventCategory")]
        public virtual ICollection<Events> Events { get; set; }
    }
}