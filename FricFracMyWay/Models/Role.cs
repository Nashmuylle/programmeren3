using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace FricFracMyWay.Models
{
    public partial class Role
    {
        public Role()
        {
        }

        [Key]
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Role-Id")]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        [FromForm(Name = "Role-Name")]
        public string Name { get; set; }

        [InverseProperty("Role")]
        public virtual ICollection<User> User { get; set; } //One To many Relationship
    }
}