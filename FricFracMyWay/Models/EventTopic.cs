using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace FricFracMyWay.Models
{
    public partial class EventTopic
    {
        public EventTopic()
        {
            
        }

        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        [FromForm(Name = "EventTopic-Name")]
        public string Name { get; set; }

        [InverseProperty("EventTopic")]
        public virtual ICollection<Events> Events { get; set; }
    }
}