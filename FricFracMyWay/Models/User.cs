using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace FricFracMyWay.Models
{
    public partial class User
    {
        public User()
        {
            
        }
        [Column(TypeName = "varchar(255)")]
        public string HashedPassword { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "User-Id")]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        [FromForm(Name = "User-Name")]
        public string Name { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "User-PersonId")]
        public int? PersonId { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "User-RoleId")]
        public int? RoleId { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Salt { get; set; }

        [ForeignKey(nameof(PersonId))]
        [InverseProperty("User")]
        public virtual Person Person { get; set; }
        [ForeignKey(nameof(RoleId))]
        [InverseProperty("User")] //InverseProperty wordt gebruikt om aan te tonen dat het in meerdere
        //relaties deelneemt.
        public virtual Role Role { get; set; }
    }
}