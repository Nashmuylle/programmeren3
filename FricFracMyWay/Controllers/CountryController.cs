using System.Linq;
using FricFracMyWay.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFracMyWay.Controllers
{
    public class CountryController : Controller
    {
        private readonly MyEntities _myEntities;

        public CountryController(MyEntities myEntities)
        {
            this._myEntities = myEntities;
        }

        // GET
        public IActionResult Index() 
        {
            ViewBag.Title = "Fric-Frac Country Index";
            return View(_myEntities.Country.ToList()); //zelf toegevoegd
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Country Inserting One";
            return View();
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id) //Het vraagteken is een nullable
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var country = _myEntities.Country.SingleOrDefault(m => m.Id == id);
            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }
        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Country Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var country = _myEntities.Country.SingleOrDefault(m => m.Id == id); //Dit gaat de Id teruggeven als hij hetzelfde is als de ID van diezelfde pagina 
            if (country == null)
            {
                return NotFound();
            }
            return View(country);
        }
        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }
        //Dit gaat de gegevens van de form naar de database sturen en dus verwereken
        [HttpPost]
        public IActionResult InsertOne(Models.Country country) 
        {
            ViewBag.Message = "Insert een land in de database";
            _myEntities.Country.Add(country);
            _myEntities.SaveChanges();
            return View("Index", _myEntities.Country.ToList());
        }
        [HttpPost]
        public IActionResult UpdateOne(Models.Country country)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _myEntities.Update(country);
                    _myEntities.SaveChanges();
                    return View("ReadingOne", country);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_myEntities.Country.Any(e => e.Id == country.Id)) //Als GEEN eender welke Id van alle countries gelijk is aan de huidige country id dan gooit hij not found
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", _myEntities.Country.ToList());
        }
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var country = _myEntities.Country.SingleOrDefault(m => m.Id == id);
            if (country == null)
            {
                return NotFound();
            }
            _myEntities.Country.Remove(country);
            _myEntities.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}