﻿using System.Linq;
using FricFracMyWay.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFracMyWay.Controllers
{
    public class RoleController : Controller
    {
        private readonly MyEntities _myEntities;

        public RoleController(MyEntities myEntities)
        {
            this._myEntities = myEntities;
        }

        // GET
        public IActionResult Index() 
        {
            ViewBag.Title = "Fric-Frac Role Index";
            return View(_myEntities.Role.ToList()); //zelf toegevoegd
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Role Inserting One";
            return View(_myEntities.Role.ToList());
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id) //Het vraagteken is een nullable
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var role = _myEntities.Role.SingleOrDefault(m => m.Id == id);
            if (role == null)
            {
                return NotFound();
            }

            return View(role);
        }
        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Role Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var role = _myEntities.Role.SingleOrDefault(m => m.Id == id); //Dit gaat de verschillende ID rows teruggeven 
            if (role == null)
            {
                return NotFound();
            }
            return View(role);
        }
        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }
        //Dit gaat de gegevens van de form naar de database sturen en dus verwereken
        [HttpPost]
        public IActionResult InsertOne(Models.Role role) 
        {
            ViewBag.Message = "Insert een land in de database";
            _myEntities.Role.Add(role);
            _myEntities.SaveChanges();
            return View("Index", _myEntities.Role.ToList());
        }
        [HttpPost]
        public IActionResult UpdateOne(Models.Role role)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _myEntities.Update(role);
                    _myEntities.SaveChanges();
                    return View("ReadingOne", role);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_myEntities.Role.Any(e => e.Id == role.Id)) //Wat doet dit juist?
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", _myEntities.Role.ToList());
        }
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var role = _myEntities.Role.SingleOrDefault(m => m.Id == id);
            if (role == null)
            {
                return NotFound();
            }
            _myEntities.Role.Remove(role);
            _myEntities.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}