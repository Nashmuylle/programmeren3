﻿using System.Linq;
using FricFracMyWay.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFracMyWay.Controllers
{
    public class EventCategoryController : Controller
    {
        private readonly MyEntities _myEntities;

        public EventCategoryController(MyEntities myEntities)
        {
            this._myEntities = myEntities;
        }

        // GET
        public IActionResult Index() 
        {
            ViewBag.Title = "Fric-Frac EventCategory Index";
            return View(_myEntities.EventCategory.ToList()); //zelf toegevoegd
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventCategory Inserting One";
            return View(_myEntities.EventCategory.ToList());
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id) //Het vraagteken is een nullable
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var eventCategory = _myEntities.EventCategory.SingleOrDefault(m => m.Id == id);
            if (eventCategory == null)
            {
                return NotFound();
            }

            return View(eventCategory);
        }
        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Eventcategory Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var eventCategory = _myEntities.EventCategory.SingleOrDefault(m => m.Id == id); //Dit gaat de verschillende ID rows teruggeven 
            if (eventCategory == null)
            {
                return NotFound();
            }
            return View(eventCategory);
        }
        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }
        //Dit gaat de gegevens van de form naar de database sturen en dus verwereken
        [HttpPost]
        public IActionResult InsertOne(Models.EventCategory eventCategory) 
        {
            ViewBag.Message = "Insert een Category in de database";
            _myEntities.EventCategory.Add(eventCategory);
            _myEntities.SaveChanges();
            return View("Index", _myEntities.EventCategory.ToList());
        }
        [HttpPost]
        public IActionResult UpdateOne(Models.EventCategory eventCategory)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _myEntities.Update(eventCategory);
                    _myEntities.SaveChanges();
                    return View("ReadingOne", eventCategory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_myEntities.EventCategory.Any(e => e.Id == eventCategory.Id)) //Wat doet dit juist?
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", _myEntities.EventCategory.ToList());
        }
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var eventCategory = _myEntities.EventCategory.SingleOrDefault(m => m.Id == id);
            if (eventCategory == null)
            {
                return NotFound();
            }
            _myEntities.EventCategory.Remove(eventCategory);
            _myEntities.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}