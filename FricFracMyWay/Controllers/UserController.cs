using System.Linq;
using FricFracMyWay.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFracMyWay.Controllers
{
    public class UserController : Controller
    {
        private readonly MyEntities _myEntities;

        public UserController(MyEntities myEntities)
        {
            this._myEntities = myEntities;
        }

        // GET
        public IActionResult Index() 
        {
            ViewBag.Title = "Fric-Frac User Index";
            return View(_myEntities.User.ToList()); //zelf toegevoegd
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac User Inserting One";
            ViewBag.Person = _myEntities.Person.ToList(); //Zelf toegevoegd om de topics te kunnen zien
            ViewBag.Role = _myEntities.Role.ToList(); //Zelf toegevoegd om de topics te kunnen zien
            return View(_myEntities.User.ToList());
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id) //Het vraagteken is een nullable
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var user = _myEntities.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            user.Person = _myEntities.Person.SingleOrDefault(m => m.Id == user.PersonId); //Dit zorgt ervoor dat we de categorie kunnen zien
            user.Role = _myEntities.Role.SingleOrDefault(m => m.Id == user.RoleId); //Hiermee gaan we de topic kunnen zien 
            return View(user);
        }
        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac User Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var user = _myEntities.User.SingleOrDefault(m => m.Id == id); //Dit gaat de verschillende ID rows teruggeven 
            if (user == null)
            {
                return NotFound();
            }
            
            user.Person = _myEntities.Person.SingleOrDefault(m => m.Id == user.PersonId); //Dit zorgt ervoor dat we de categorie kunnen zien
            user.Role = _myEntities.Role.SingleOrDefault(m => m.Id == user.RoleId); //Hiermee gaan we de topic kunnen zien 
            ViewBag.Person = _myEntities.Person.ToList();
            ViewBag.Role = _myEntities.Role.ToList();
            return View(user);
        }
        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }
        //Dit gaat de gegevens van de form naar de database sturen en dus verwereken
        [HttpPost]
        public IActionResult InsertOne(Models.User user) 
        {
            ViewBag.Message = "Insert een land in de database";
            _myEntities.User.Add(user);
            _myEntities.SaveChanges();
            return View("Index", _myEntities.User.ToList());
        }
        [HttpPost]
        public IActionResult UpdateOne(Models.User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _myEntities.Update(user);
                    _myEntities.SaveChanges();
                    return View("ReadingOne", user);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_myEntities.User.Any(e => e.Id == user.Id)) //Wat doet dit juist?
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", _myEntities.User.ToList());
        }
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var user = _myEntities.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            _myEntities.User.Remove(user);
            _myEntities.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}