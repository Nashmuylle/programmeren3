using System.Linq;
using FricFracMyWay.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFracMyWay.Controllers
{
    public class EventsController : Controller
    {
        private readonly MyEntities _myEntities;

        public EventsController(MyEntities myEntities)
        {
            this._myEntities = myEntities;
        }

        // GET
        public IActionResult Index() 
        {
            ViewBag.Title = "Fric-Frac Events Index";
            return View(_myEntities.Events.ToList()); //zelf toegevoegd
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Events Inserting One";
            ViewBag.EventTopic = _myEntities.EventTopic.ToList(); //Zelf toegevoegd om de topics te kunnen zien
            ViewBag.EventCategory = _myEntities.EventCategory.ToList(); //Zelf toegevoegd om de topics te kunnen zien 
            return View();
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id) //Het vraagteken is een nullable
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var events = _myEntities.Events.SingleOrDefault(m => m.Id == id);
            if (events == null)
            {
                return NotFound();
            }

            events.EventCategory = _myEntities.EventCategory.SingleOrDefault(m => m.Id == events.EventCategoryId); //Dit zorgt ervoor dat we de categorie kunnen zien
            events.EventTopic = _myEntities.EventTopic.SingleOrDefault(m => m.Id == events.EventTopicId); //Hiermee gaan we de topic kunnen zien 
            return View(events);
        }
        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac events Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var events = _myEntities.Events.SingleOrDefault(m => m.Id == id); //Dit gaat de Id teruggeven als hij hetzelfde is als de ID van diezelfde pagina 
            if (events == null)
            {
                return NotFound();
            }
            
            events.EventCategory = _myEntities.EventCategory.SingleOrDefault(m => m.Id == events.EventCategoryId); //Dit zorgt ervoor dat we de categorie kunnen zien
            events.EventTopic = _myEntities.EventTopic.SingleOrDefault(m => m.Id == events.EventTopicId); //Hiermee gaan we de topic kunnen zien 
            ViewBag.EventTopic = _myEntities.EventTopic.ToList();
            ViewBag.EventCategory = _myEntities.EventCategory.ToList();
            return View(events);
        }
        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }
        //Dit gaat de gegevens van de form naar de database sturen en dus verwereken
        [HttpPost]
        public IActionResult InsertOne(Models.Events events) 
        {
            ViewBag.Message = "Insert een land in de database";
            _myEntities.Events.Add(events);
            _myEntities.SaveChanges();
            return View("Index", _myEntities.Events.ToList());
        }
        [HttpPost]
        public IActionResult UpdateOne(Models.Events events)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _myEntities.Update(events);
                    _myEntities.SaveChanges();
                    return View("ReadingOne", events);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_myEntities.Events.Any(e => e.Id == events.Id)) //Als GEEN eender welke Id van alle countries gelijk is aan de huidige event id dan gooit hij not found
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", _myEntities.Events.ToList());
        }
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var events = _myEntities.Events.SingleOrDefault(m => m.Id == id);
            if (events == null)
            {
                return NotFound();
            }
            _myEntities.Events.Remove(events);
            _myEntities.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}