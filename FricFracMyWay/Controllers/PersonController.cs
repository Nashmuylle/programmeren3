﻿using System.Linq;
using FricFracMyWay.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFracMyWay.Controllers
{
    public class PersonController : Controller
    {
        private readonly MyEntities _myEntities;

        public PersonController(MyEntities myEntities)
        {
            this._myEntities = myEntities;
        }

        // GET
        public IActionResult Index() 
        {
            ViewBag.Title = "Fric-Frac Person Index";
            return View(_myEntities.Person.ToList()); //zelf toegevoegd
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Person Inserting One";
            ViewBag.Countries = _myEntities.Country.ToList();
            return View();
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id) //Het vraagteken is een nullable
        {
            ViewBag.Message = "Insert een Persoon in de database";
            if (id == null)
            {
                return NotFound();
            }

            var person = _myEntities.Person.SingleOrDefault(m => m.Id == id);
            if (person == null)
            {
                return NotFound();
            }
            person.Country = _myEntities.Country.SingleOrDefault(m => m.Id == person.CountryId);
            return View(person);
        }
        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Person Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var person = _myEntities.Person.SingleOrDefault(m => m.Id == id); //Dit gaat de verschillende ID rows teruggeven 
            if (person == null)
            {
                return NotFound();
            }
            person.Country = _myEntities.Country.SingleOrDefault(m => m.Id == person.CountryId);
            ViewBag.Countries = _myEntities.Country.ToList();
            return View(person);
        }
        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }
        //Dit gaat de gegevens van de form naar de database sturen en dus verwereken
        [HttpPost]
        public IActionResult InsertOne(Models.Person person) 
        {
            ViewBag.Message = "Insert een land in de database";
            _myEntities.Person.Add(person);
            _myEntities.SaveChanges();
            return View("Index", _myEntities.Person.ToList());
        }
        [HttpPost]
        public IActionResult UpdateOne(Models.Person person)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _myEntities.Update(person);
                    _myEntities.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_myEntities.Person.Any(e => e.Id == person.Id)) //Wat doet dit juist?
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", _myEntities.Person.ToList());
        }
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var person = _myEntities.Person.SingleOrDefault(m => m.Id == id);
            if (person == null)
            {
                return NotFound();
            }
            _myEntities.Person.Remove(person);
            _myEntities.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}