﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FricFracMyWay.Controllers
{
    public class PostcodeController : Controller
    {
        // GET: /<controller>/

        public PostcodeController(FricFracMyWay.DAL.IPostcode dalService)
        {
            this.dalService = dalService;
        }

        public IActionResult Index()
        {
            
            dalService.ReadAll();
            return View(FricFracMyWay.Models.Postcode.List);
        }

        private readonly FricFracMyWay.DAL.IPostcode dalService;

      
    }
}
