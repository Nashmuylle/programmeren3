﻿using System.Linq;
using FricFracMyWay.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFracMyWay.Controllers
{
    public class EventTopicController : Controller
    {
        private readonly MyEntities _myEntities;

        public EventTopicController(MyEntities myEntities)
        {
            this._myEntities = myEntities;
        }

        // GET
        public IActionResult Index() 
        {
            ViewBag.Title = "Fric-Frac EventTopic Index";
            return View(_myEntities.EventTopic.ToList()); //zelf toegevoegd
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventTopic Inserting One";
            return View();
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id) //Het vraagteken is een nullable
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var eventTopic = _myEntities.EventTopic.SingleOrDefault(m => m.Id == id);
            if (eventTopic == null)
            {
                return NotFound();
            }

            return View(eventTopic);
        }
        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Eventtopic Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var eventTopic = _myEntities.EventTopic.SingleOrDefault(m => m.Id == id); //Dit gaat de verschillende ID rows teruggeven 
            if (eventTopic == null)
            {
                return NotFound();
            }
            return View(eventTopic);
        }
        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }
        //Dit gaat de gegevens van de form naar de database sturen en dus verwereken
        [HttpPost]
        public IActionResult InsertOne(Models.EventTopic eventTopic) 
        {
            ViewBag.Message = "Insert een land in de database";
            _myEntities.EventTopic.Add(eventTopic);
            _myEntities.SaveChanges();
            return View("Index", _myEntities.EventTopic.ToList());
        }
        [HttpPost]
        public IActionResult UpdateOne(Models.EventTopic eventTopic)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _myEntities.Update(eventTopic);
                    _myEntities.SaveChanges();
                    return View("ReadingOne", eventTopic);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_myEntities.EventTopic.Any(e => e.Id == eventTopic.Id)) //Wat doet dit juist?
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", _myEntities.EventTopic.ToList());
        }
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var eventTopic = _myEntities.EventTopic.SingleOrDefault(m => m.Id == id);
            if (eventTopic == null)
            {
                return NotFound();
            }
            _myEntities.EventTopic.Remove(eventTopic);
            _myEntities.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}