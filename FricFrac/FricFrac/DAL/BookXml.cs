﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using BookApp.BLL;

namespace BookApp.DAL
{
    public class BookXml : IBook
    {
        public BLL.Book Book { get; set; }
        public string Message { get; set; }
        private string connectionString = @"Data/Book";
        public string ConnectionString
        {
            get
            {
                return connectionString + ".xml";
            }
            set
            {
                connectionString = value;
            }
        }
        public BookXml(BLL.Book book)
        {
            Book = book;
        }
        public BookXml(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public bool Create()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(BLL.Book[]));
                TextWriter writer = new StreamWriter(ConnectionString);
                BLL.Book[] books = Book.List.ToArray();
                serializer.Serialize(writer, books);
                writer.Close();
                Message = $"Bestand {ConnectionString} is met succes geserialiseerd.";
                return true;
            }
            catch (Exception e)
            {
                Message = $"Het bestand {ConnectionString} is niet gedeserialiseerd.\nFoutmelding {e.Message}.";
                return false;
            }
        }
        public bool ReadAll()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(BLL.Book[]));
                StreamReader file = new System.IO.StreamReader(ConnectionString);
                BLL.Book[] books = (BLL.Book[])serializer.Deserialize(file);
                file.Close();
                Book.List = new List<BLL.Book>(books);
                Message = $"Bestand {ConnectionString} is met succes gedeserialiseerd.";
                return true;
            }
            catch (Exception e)
            {
                Message = $"Het bestand {ConnectionString} is niet gedeserialiseerd.\nFoutmelding {e.Message}.";
                return false;
            }
        }
    }
}
