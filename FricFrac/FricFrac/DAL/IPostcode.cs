﻿using System;
namespace PostcodeApp.DAL
{
    public interface IPostcode
    {
        BLL.Postcode Postcode { get; set; }
        string Message { get; set; }
        string ConnectionString { get; set; }
        bool Create();
        bool ReadAll();
    }
}
