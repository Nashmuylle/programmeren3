﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookApp.DAL
{
    public interface IBook
    {
        BLL.Book Book { get; set; }
        string Message { get; set; }
        string ConnectionString { get; set; }
        bool Create();
        bool ReadAll();
    }
}
