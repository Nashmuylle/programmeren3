﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Fric_Frac.Controllers
{
    public class PostcodeController : Controller
    {
        // GET: /<controller>/

        public PostcodeController(PostcodeApp.DAL.IPostcode dalService)
        {
            this.dalService = dalService;
        }

        public IActionResult Index()
        {
            
            dalService.ReadAll();
            return View(PostcodeApp.BLL.Postcode.List);
        }

        private readonly PostcodeApp.DAL.IPostcode dalService;

      
    }
}
