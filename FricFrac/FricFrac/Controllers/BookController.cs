﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Fric_Frac.Controllers
{
    public class BookController : Controller
    {
        public BookController(BookApp.DAL.IBook dalService)
        {
            this.dalService = dalService;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            dalService.ReadAll();
            return View(BookApp.BLL.Book.List);
        }
        private readonly BookApp.DAL.IBook dalService;
    }
}
