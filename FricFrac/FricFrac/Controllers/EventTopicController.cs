﻿using Microsoft.AspNetCore.Mvc;

namespace FricFrac.Controllers
{
    public class EventTopicController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac EventTopic Index";
            return View();
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventTopic Inserting One";
            return View();
        }

        public IActionResult ReadingOne()
        {
            ViewBag.Title = "Fric-frac EventTopic Reading One";
            return View();
        }

        public IActionResult UpdatingOne()
        {
            ViewBag.Title = "Fric-frac EventTopic Updating One";
            return View();
        }
    }
}