﻿using Microsoft.AspNetCore.Mvc;

namespace FricFrac.Controllers
{
    public class AdminController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            // wordt gebruikt in het head->title element
            // de Master Page
            ViewBag.Title = "Fric-frac Admin";
            return View();
        }
    }
}