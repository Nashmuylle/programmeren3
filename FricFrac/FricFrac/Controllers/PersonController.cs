﻿using Microsoft.AspNetCore.Mvc;

namespace FricFrac.Controllers
{
    public class PersonController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Person Index";
            return View();
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Person Inserting One";
            return View();
        }

        public IActionResult ReadingOne()
        {
            ViewBag.Title = "Fric-frac Person Reading One";
            return View();
        }

        public IActionResult UpdatingOne()
        {
            ViewBag.Title = "Fric-frac Person Updating One";
            return View();
        }
    }
}