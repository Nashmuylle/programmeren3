﻿using Microsoft.AspNetCore.Mvc;

namespace FricFrac.Controllers
{
    public class RoleController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Role Index";
            return View();
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Role Inserting One";
            return View();
        }

        public IActionResult ReadingOne()
        {
            ViewBag.Title = "Fric-frac Role Reading One";
            return View();
        }

        public IActionResult UpdatingOne()
        {
            ViewBag.Title = "Fric-frac Role Updating One";
            return View();
        }
    }
}