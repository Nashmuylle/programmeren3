﻿using Microsoft.AspNetCore.Mvc;

namespace FricFrac.Controllers
{
    public class EventCategoryController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac EventCategory Index";
            return View();
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventCategory Inserting One";
            return View();
        }

        public IActionResult ReadingOne()
        {
            ViewBag.Title = "Fric-frac EventCategory Reading One";
            return View();
        }

        public IActionResult UpdatingOne()
        {
            ViewBag.Title = "Fric-frac EventCategory Updating One";
            return View();
        }
    }
}