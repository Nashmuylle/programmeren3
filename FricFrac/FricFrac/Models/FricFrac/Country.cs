﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fric_Frac.Models.FricFrac
{
    public partial class Country
    {
        public Country()
        {
            Person = new HashSet<Person>();
        }

        [Column(TypeName = "varchar(2)")]
        public string Code { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        [StringLength(256)]
        public string Desc { get; set; }

        [InverseProperty("Country")]
        public virtual ICollection<Person> Person { get; set; }
    }
}
