﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fric_Frac.Models.FricFrac
{
    public partial class User
    {
        [Column(TypeName = "varchar(255)")]
        public string HashedPassword { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        [Column(TypeName = "int(11)")]
        public int? PersonId { get; set; }
        [Column(TypeName = "int(11)")]
        public int? RoleId { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Salt { get; set; }

        [ForeignKey(nameof(PersonId))]
        [InverseProperty("User")]
        public virtual Person Person { get; set; }
        [ForeignKey(nameof(RoleId))]
        [InverseProperty("User")]
        public virtual Role Role { get; set; }
    }
}
