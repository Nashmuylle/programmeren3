﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fric_Frac.Models.FricFrac
{
    public partial class Person
    {
        public Person()
        {
            User = new HashSet<User>();
        }

        [Column(TypeName = "varchar(255)")]
        public string Address1 { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Address2 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Birthday { get; set; }
        [Column(TypeName = "varchar(80)")]
        public string City { get; set; }
        [Column(TypeName = "int(11)")]
        public int? CountryId { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Email { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string FirstName { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        public string LastName { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Password { get; set; }
        [Column(TypeName = "varchar(25)")]
        public string Phone1 { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string PostalCode { get; set; }
        [Column(TypeName = "int(11)")]
        public int? Rating { get; set; }

        [ForeignKey(nameof(CountryId))]
        [InverseProperty("Person")]
        public virtual Country Country { get; set; }
        [InverseProperty("Person")]
        public virtual ICollection<User> User { get; set; }
    }
}
