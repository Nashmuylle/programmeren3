﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fric_Frac.Models.FricFrac
{
    public partial class EventCategory
    {
        public EventCategory()
        {
            Event = new HashSet<Event>();
        }

        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        public string Name { get; set; }

        [InverseProperty("EventCategory")]
        public virtual ICollection<Event> Event { get; set; }
    }
}
