﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace P3GegevensStringbuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Users\paren\Documents\School\Programmeren\programmeren3\P3GegevensStringbuilder\big.txt";
            string[] lines = System.IO.File.ReadAllLines(path);
            StringBuilder sb = new StringBuilder();
            Stopwatch stopwatchConcatenation = new Stopwatch();
            Stopwatch stopwatchStringbuilder = new Stopwatch();
            Console.WriteLine("We beginnen met Concatenation te testen!");
            string output = "";
            //ConcatenationTest
            stopwatchConcatenation.Start();
            foreach (string line in lines)
            {
                if (line.Length > 0)
                {
                    output += line[line.Length - 1] + "\n";

                }
                    output += "\n";
                
            }
            stopwatchConcatenation.Stop();
            TimeSpan concatenationTime = stopwatchConcatenation.Elapsed; //Dit is de variabele die de tijd vasthoudt
            Console.WriteLine(concatenationTime);
            //StringbuilderTest
            stopwatchStringbuilder.Start();
            foreach (string line in lines)
            {
                if (line.Length > 0)
                {
                    sb.Append(line[line.Length -1]);

                }
                sb.Append("\n");
            }
            stopwatchStringbuilder.Stop();
            TimeSpan stringbuilderTime = stopwatchStringbuilder.Elapsed;
            Console.WriteLine($"Concatenation time is{concatenationTime}");
            Console.WriteLine($"Stringbuilder time is {stringbuilderTime}");
        }
    }
}
