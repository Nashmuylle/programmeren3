﻿using System;
using System.Text;
using System.Globalization;

namespace P3MooieTabel
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Wat is je voornaam? (max 20 tekens)");
            String name = Console.ReadLine();
            Console.WriteLine("Wat is je familienaam? (max 20 tekens)");
            string surName = Console.ReadLine();
            Console.WriteLine("Hoe groot ben je? (in cm) ");
            int length = int.Parse(Console.ReadLine());
            Console.WriteLine("Wat is je (toekomstig) brutoloon (als een geheel getal in jouw munteenheid)?");
            int wage = int.Parse(Console.ReadLine());
            DateTime inputDate = DateTime.Now; //Datum van vandaag

            Console.WriteLine($"| {"Voornaam", -20} | {"familienaam", -20} |");
            Console.WriteLine($"| {name, -20} | {surName, -20} | {length, 6} | {wage, 11:c} | {inputDate.ToString("D", new CultureInfo("fr-FR")),27} |");


        }
    }
}
