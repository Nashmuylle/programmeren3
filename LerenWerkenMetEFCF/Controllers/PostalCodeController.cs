using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LerenWerkenMetEFCF.Models;

namespace LerenWerkenMetEFCF.Controllers
{
    public class PostalCodeController : Controller
    {
        private readonly MyEntities db;

        public PostalCodeController(MyEntities db)
        {
            this.db = db;
        }
        // GET
        [HttpPost]
        public IActionResult Insert(Models.PostalCode postalCodes)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Message = "Insert een postcode in de database";
                db.PostalCodes.Add(postalCodes);
                db.SaveChanges();
            }
            else
            {
                ModelState.AddModelError("Ongeldige gegevens", "Postcode en plaatsnaam moeten ingevuld zijn.");
            }
            return View("Inserting", postalCodes);
        }
    }
}