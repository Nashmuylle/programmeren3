﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LerenWerkenMetEFCF.Models;

namespace LerenWerkenMetEFCF.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private MyEntities context; //Dit object zal ons toegang geven tot de database. Een dependency

        public HomeController(ILogger<HomeController> logger, MyEntities context) //Hier gaat de injectie toegepast worden.(dit is dependency injection)
        {
            _logger = logger;
            this.context = context;
        }

        public IActionResult Index()
        {
            ViewBag.Message = "Oefenwebsite EFCF"; //Dit wordt getoond
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
        public IActionResult CreateDatabase()
        {
            ViewBag.Message = "Oefenwebsite EFCF een database maken";
            ViewBag.DatabaseMessage = "Database op het punt aan te maken";
            try
            {
                context.Database.EnsureCreated(); //Dit gaat via de database die in context zien of ze zeker is gemaakt. Bij Startup
                ViewBag.DatabaseMessage = "Database gemaakt!";
            }
            catch (Exception e)
            {
                ViewBag.DatabaseMessage = e.Message;
            }
            return View("Index");
        }
    }
}