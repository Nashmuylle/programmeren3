using System.ComponentModel.DataAnnotations;

namespace LerenWerkenMetEFCF.Models
{
    public class PostalCode
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        [MinLength(4, ErrorMessage = "Postcode bestaat uit minimum 4 cijfers.")]
        [MaxLength(4, ErrorMessage = "Postcode bestaat uit maximum 4 cijfers.")]
        public string Code { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Postcode bestaat uit maximum 50 cijfers.")]
        public string Location { get; set; }
    }
}