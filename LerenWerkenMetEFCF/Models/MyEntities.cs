// Dit allemaal zorgt voor acces aan de database
using Microsoft.EntityFrameworkCore;

namespace LerenWerkenMetEFCF.Models
{
    public class MyEntities : DbContext
    {
        public MyEntities(DbContextOptions<MyEntities> options) //Dit geeft de configuratie file mee
            : base(options)
        {
        }
        public virtual DbSet<PostalCode> PostalCodes { get; set; }
    }
}