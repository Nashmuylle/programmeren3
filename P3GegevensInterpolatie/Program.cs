﻿using System;

namespace P3GegevensInterpolatie
{
    class Program
    {
        static void Main(string[] args)
        {
            int var1;
            int var2;
            Console.WriteLine("Gelieve 2 getallen op te geven waarvan we het product gaan berekenen");
            Console.WriteLine("Getal 1");
            var1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Getal 2");
            var2 = int.Parse(Console.ReadLine());
            Console.WriteLine($"Het product van {nameof(var1)} en {nameof(var2)} is {var1 * var2}");
        } //nameof toont de naam van de variabele
    }
}
