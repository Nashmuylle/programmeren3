﻿using System;

namespace PostcodeApp
{
    public class App
    {
        private readonly Dal.IPostcode postcodeDal;

        public App(Dal.IPostcode postcodeDal)
        {
            this.postcodeDal = postcodeDal;
        }

        public void Run()
        {
            Console.WriteLine("De Postcode App Generic");
            postcodeDal.ReadAll();
            Console.WriteLine(postcodeDal.Message);
            View.PostcodeConsole view = new View.PostcodeConsole();
            view.List();
            // serialize to another file
            postcodeDal.ConnectionString = "Data/Postcode2";
            postcodeDal.Create();
            Console.WriteLine(postcodeDal.Message);
        }
    }
}