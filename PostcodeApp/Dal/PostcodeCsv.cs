﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using PostcodeApp.Bll;

namespace PostcodeApp.Dal
{
    class PostcodeCsv : IPostcode
    {
        public string Message { get; set; }
        private string connectionString = @"Data/Postcode.csv";
        
        public string ConnectionString
        {
            get
            {
                return connectionString;
            }
            set
            {
                connectionString = value;
            }
        }
        public char Seperator { get; set; } = ';';
        public bool ReadAll()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = ConnectionString; //locatie naar bestand
            if (bestand.Lees())
            {
                string[] postcodes = bestand.Text.Split('\n');
                try
                {
                    List<Bll.Postcode> list = new List<Bll.Postcode>(); 
                    foreach (string s in postcodes)
                    {
                        if (s.Length > 0)
                        {
                            list.Add(ToObject(s));
                        }
                    }
                    Postcode.List = list;
                    Message = $"Het bestand {ConnectionString} is gedesialiseerd!";
                    return true;
                }
                catch (Exception e)
                {
                    Message = $"Bestand {ConnectionString} is niet gedesialiseerd. \nFoutmelding {e.Message}.";
                    return false;
                }
            }
            else
            {
                Message = $"Bestand {ConnectionString} is niet gedeserialiseerd. \n Foutmeldiing {bestand.Melding}";
                return false;
            }
        }
        private Bll.Postcode ToObject(string line)
        {
            Bll.Postcode postcode = new Bll.Postcode();
            string[] values = line.Split(Seperator);
            postcode.Code = values[0];
            postcode.Plaats = values[1];
            postcode.Provincie = values[2];
            postcode.Localite = values[3];
            postcode.Province = values[4];
            return postcode;
        }
        public bool Create()
        {
            try
            {
                TextWriter writer = new StreamWriter(connectionString);
                foreach (Bll.Postcode item in Postcode.List)
                {
                    writer.WriteLine("{0}{5}{1}{5}{2}{5}{3}{5}{4}",
                        item?.Code,
                        item?.Plaats,
                        item?.Provincie,
                        item?.Localite,
                        item?.Province,
                        Seperator);
                }
                writer.Close();
                Message = $"Het bestand met de naam {ConnectionString} is gemaakt!";
                return true;
            }
            catch (Exception e)
            {
                Message = $"Bestand met naam {ConnectionString} niet gemaakt.\nFoutmelding {e.Message}.";
                return false;
            }
        }
        public bool Create(char seperator = ';')
        {
            Seperator = seperator;
            return Create();
        }
    }
}
