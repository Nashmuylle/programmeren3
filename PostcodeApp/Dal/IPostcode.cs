﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostcodeApp.Dal
{
    public interface IPostcode
    {
        //Een Postcode BLL object om opgehaalde waarden
        // in op te slagen
        string Message { get; set; }
        string ConnectionString { get; set; }
        bool Create();
        bool ReadAll();
    }

}
