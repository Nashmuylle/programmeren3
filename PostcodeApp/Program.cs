﻿using System;
using PostcodeApp.Bll;
using PostcodeApp.Dal;
using PostcodeApp.View;

namespace PostcodeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TryOutCsv();
            TryOutXml();
            Console.ReadLine();
        }

        static void TryOutCsv()
        {
            Console.WriteLine("De Postcode App CSV");
            Bll.Postcode postcode = new Bll.Postcode();
            Dal.PostcodeCsv postcodeCsv = new Dal.PostcodeCsv();
            // de seperator staat standaard op ;
            // in het Postcode.csv bestand is dat |
            postcodeCsv.Seperator = '|';
            postcodeCsv.ReadAll();
            Console.WriteLine(postcodeCsv.Message);
            View.PostcodeConsole view = new View.PostcodeConsole();
            view.List();
            // serialize postcodes met een andere separator
            // naar ander bestand
            postcodeCsv.ConnectionString = "Data/PostcodeSemicolon.csv";
            postcodeCsv.Create(';');
            Console.WriteLine(postcodeCsv.Message);
        }
        static void TryOutXml()
        {
            Console.WriteLine("De Postcode App XML");
            Bll.Postcode postcode = new Bll.Postcode();
            Dal.PostcodeXml postcodeXml = new Dal.PostcodeXml();
            postcodeXml.ReadAll();
            Console.WriteLine(postcodeXml.Message);
            View.PostcodeConsole view = new View.PostcodeConsole();
            view.List();
            // serialize naar ander bestand
            postcodeXml.ConnectionString = "Data/Postcode2.xml";
            postcodeXml.Create();
            Console.WriteLine(postcodeXml.Message);
        }
    }
}