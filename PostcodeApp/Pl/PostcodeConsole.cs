﻿using System;

namespace PostcodeApp.View
{
    class PostcodeConsole
    {

        public void List()
        {
            foreach (Bll.Postcode postcode in Bll.Postcode.List)
            {
                // One of the most versatile and useful additions to the C# language in version 6
                // is the null conditional operator ?.Post           
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}",
                    postcode?.Code,
                    postcode?.Plaats,
                    postcode?.Provincie,
                    postcode?.Localite,
                    postcode?.Province);
            }
        }
    }
}