﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookApp.Bll
{
    class Book
    {
        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private string year;
        public string Year
        {
            get { return year; }
            set { year = value; }
        }

        private string city;
        public string City
        {
            get { return city; }
            set { city = value; }
        }

        private string publisher;
        public string Publisher
        {
            get { return publisher; }
            set { publisher = value; }
        }

        private string author;
        public string Author
        {
            get { return author; }
            set { author = value; }
        }
        private string edition;
        public string Edition
        {
            get 
        }

        private List<Postcode> list;
        public List<Postcode> List
        {
            get { return list; }
            set { list = value; }
        }
    }
}
