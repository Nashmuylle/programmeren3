﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H12Ziekenhuis
{
    public class Patient
    {
        protected string name;
        protected int duration;
        public Patient(string n, int d)
        {
            this.name = n;
            this.duration = d;
        }
        public virtual float ComputeCost()
        {
            return 50 + (20 * this.duration);
        }

        public virtual void ShowInfo()
        {
            Console.WriteLine($"{this.name}, is een gewone patiënt die {this.duration} uur in het ziekenhuis heeft gelegen, betaalt €{this.ComputeCost()}");
        }
    }

}
