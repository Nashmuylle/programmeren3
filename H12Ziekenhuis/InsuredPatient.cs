﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H12Ziekenhuis
{
    public class InsuredPatient: Patient
    {
        public InsuredPatient(string n, int d) : base(n,d)
        {
        }
        public override float  ComputeCost()
        {
            return base.ComputeCost() * 0.9f;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"{this.name}, is een verzekerde patiënt die {this.duration} uur in het ziekenhuis heeft gelegen, betaalt €{this.ComputeCost()}");
        }
    }

}
