﻿using System;

namespace H12Ziekenhuis
{
    class Program
    {
        static void Main(string[] args)
        {
            Patient vincent = new Patient("Vincent", 12);
            vincent.ShowInfo();
            Patient tim = new InsuredPatient("Tim", 12);
            tim.ShowInfo();
           
        }
    }
}
