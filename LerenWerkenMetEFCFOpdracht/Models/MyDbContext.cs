// Dit allemaal zorgt voor acces aan de database
using Microsoft.EntityFrameworkCore;

namespace LerenWerkenMetEFCFOpdracht.Models
{
    public class MyDbContext : DbContext //DbContext gaat ervoor zorgen dat we de database gaan kunnen aanpassen en deze aanpassing dan ook worden opgeslagen
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) //Dit geeft de configuratie file mee
            : base(options) //Here we pass the options to the base class
        {
        }
        public virtual DbSet<CreditCard> CreditCards { get; set; } //DbSet representeert een entiteit voor CRUD operations (Create; Read; Update; Delete)
    } 
}