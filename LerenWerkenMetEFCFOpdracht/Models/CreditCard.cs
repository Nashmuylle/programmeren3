using System.ComponentModel.DataAnnotations;

namespace LerenWerkenMetEFCFOpdracht.Models
{
    public class CreditCard
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Postcode bestaat uit maximum 4 cijfers.")]
        public string Naam { get; set; }
        [Required]
        [MinLength(10, ErrorMessage = "Een nummer is altijd 10 cijfers lang")]
        [MaxLength(10, ErrorMessage = "Een nummer is altijd 10 cijfers lang.")]
        public int Nummer { get; set; }
    }
}