using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LerenWerkenMetEFCFOpdracht.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using LerenWerkenMetEFCFOpdracht.Models;

namespace LerenWerkenMetEFCFOpdracht
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<MyDbContext>(o => o.UseMySql(Configuration.GetConnectionString("MyEntities"))); //Deze service hebben we zelf toegevoegd. Dit zal zeggen dat we de service Dbcontext gaan gebruiken met myEntities.
        }    //Hierboven wordt ook gezegd welke soort databank en de connectionstring ervoor meegegeven.
            // Er wordt een lambda functie gebruikt om de opties door te geven. (options => options.TeGebruikenDatabank(Conifguratie klasse als parameter)
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}