﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H8Figuren
{
    class Rectangle
    {
        public Rectangle()
        {

        }
        public float Width
        {
            get
            {
                return this.Width;
            }
            set
            {
                if(value < 1)
                {
                    throw new Exception();
                }
                this.Width = value;
            }
        }
        public float Height
        {
            get
            {
                return this.Height;
            }
            set
            {
                if (value < 1)
                {
                    throw new Exception();
                }
                this.Height = value;
            }
        }
        public ComputeSurface()
        {
            return this.Width * this.Height;
        }
    }
}
